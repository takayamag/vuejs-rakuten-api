// axios: https://github.com/axios/axios
import axios from 'axios'

export function openExternalSite (url) {
  window.open(url)
}

// yyyy-MM-dd の形式で文字列を返すメソッド
export function getDateAsYmd (offset = 0) {
  let d = new Date()

  // 当日からの差分の日付をセットする
  if (offset !== 0) {
    d.setDate(d.getDate() + offset)
  }

  // ローカルタイムで処理する
  let year = d.getFullYear()
  let month = d.getMonth() + 1
  let day = d.getDate()

  // yyyy-MM-dd の形式
  return (year + '-' + month + '-' + day)
}

// 数値を3桁ごとにカンマ区切りにする
// 変換出来ない値の時は 'ー' の文字列を返す
export function chargeToLocalString (charge) {
  return charge ? charge.toLocaleString() : 'ー'
}

// 消費税込みで数値を3桁ごとにカンマ区切りにする
// 変換出来ない値の時は 'ー' の文字列を返す
export function chargeToLocalStringWithTax (charge) {
  if (charge) {
    const CURRENT_TAX = 1.08
    const CHARGE_WITH_TAX = parseInt(charge) * CURRENT_TAX
    return Math.round(CHARGE_WITH_TAX).toLocaleString()
  } else {
    return 'ー'
  }
}

// axiosのライブラリを使用して楽天APIへリクエストを送る
export function requestRakutenApi (apiPath, params) {
  let instance = axios.create({
    baseURL: 'https://app.rakuten.co.jp/services/api',
    timeout: 3000,
    method: 'GET',
    responseType: 'json', // レスポンスはjsonオブジェクト固定
    params: params
  })
  return instance.get(apiPath, {})
    .then(response =>
      response.data
    ).catch(error => {
      console.log(error.response.status)
      console.log(error.response.data)
      return null
    })
}
