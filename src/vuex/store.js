import Vue from 'vue'
import Vuex from 'vuex'
import HotelSearch from './modules/hotel-search'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    HotelSearch
  }
})
export default store
