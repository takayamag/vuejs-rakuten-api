import { getDateAsYmd, requestRakutenApi } from '../../helpers/utils'
import * as types from './mutation-types'

const HotelSearch = {
  namespaced: true, // ネームスペースをtrueにすることで、HotelSearch/{action_name}
  state: {
    searchMode: 0,  // KeywordHotelSearch: 0, SimpleHotelSearch: 1
    areaCode: [],   // 楽天トラベル地区コードAPIのレスポンスを保持する
    conditions: {
      keyword: '',  // 入力したキーワードを保持する
      area: {
        middle: '', // 選択中の都道府県コードを保持する
        small: '',  // 選択中の市区町村コードを保持する
        detail: ''  // 選択中の駅や詳細地域コードを保持する
      },
      adultNum: 2,   // 宿泊時の人数を保持する (デフォルト値は2)
      visitDuration: // 宿泊日データをyyyy-MM-dd形式で保持する
      [
        getDateAsYmd(), // 今日の日付をチェックインのデフォルト値とする
        getDateAsYmd(1) // 翌日の日付をチェックアウトのデフォルト値とする
      ],
      currentPage: 1 // 現在のページインデックスを保持する (デフォルト値は1)
    },
    result: {
      pagingInfo: {}, // 検索結果の総数などを保持する
      hotels: []      // 検索結果の宿泊施設情報などを保持する
    }
  },
  getters: {
    // 各stateを参照するためのgetterを指定する
    conditions: state => state.conditions,
    pagingInfo: state => state.result.pagingInfo,
    hotels: state => state.result.hotels,
    areaCode: state => state.areaCode
  },
  mutations: {
    // 楽天トラベル地区コードAPIのレスポンスをコミットする
    // 基本的にアプリケーション起動時に一度だけ取得する
    [types.UPDATE_AREA_CODE] (state, data) {
      state.areaCode = data
    },
    // 宿泊施設のデータや検索結果のサマリーなどをコミットする
    [types.UPDATE_RESULT_STATE] (state, data) {
      state.result.hotels = data.hotels
      state.result.pagingInfo = data.pagingInfo
    },
    // 現在のページインデックスをコミットする
    [types.UPDATE_CURRENT_PAGE] (state, currentPage) {
      state.conditions.currentPage = currentPage
    },
    // 現在の検索モード(APIの使い分け)の状態をコミットする
    [types.UPDATE_SEARCH_MODE] (state, id) {
      state.conditions.searchMode = id
    }
  },
  actions: {
    // 楽天トラベル地区コードAPIへリクエストを送信し、stateを更新する
    getAreaClass ({ commit, state }) {
      let params = {
        applicationId: process.env.RAKUTEN_API_KEY,
        format: 'json',
        formatVersion: '2',
        elements: 'middleClasses' // 都道府県、市区町村、詳細地域に絞ってレスポンスを返すように指定する
      }
      const API_PATH = '/Travel/GetAreaClass/20131024'
      requestRakutenApi(API_PATH, params)
        .then(data => {
          if (data !== null) {
            commit(types.UPDATE_AREA_CODE, data)
          }
        })
    },
    // 楽天トラベルキーワード検索APIへリクエストを送信してレスポンスを取得する
    keywordHotelSearch ({ commit, state }) {
      let params = {
        applicationId: process.env.RAKUTEN_API_KEY,
        format: 'json',
        keyword: state.conditions.keyword,  // フォームで入力したキーワードの文字列を渡す
        formatVersion: '2',
        datumType: '1',                     // 世界測地系、単位は度
        hits: '10',                         // 最大10件のレスポンスを得る(最大30件まで指定出来る)
        page: state.conditions.currentPage, // どのページを起点に検索するかを指定する
        responseType: 'middle',             // 中程度の情報量を返却することをリクエストする
        sort: 'standard'                    // レスポンスはキーワード適中率が高い順でソートする
      }
      const API_PATH = '/Travel/KeywordHotelSearch/20170426'
      requestRakutenApi(API_PATH, params)
        .then(data => {
          if (data !== null) {
            commit(types.UPDATE_RESULT_STATE, data)
          }
        })
    },
    // 楽天トラベル空室検索APIへリクエストを送信してレスポンスを取得する
    simpleHotelSearch ({ commit, state }) {
      let params = {
        applicationId: process.env.RAKUTEN_API_KEY,
        format: 'json',
        largeClassCode: 'japan',                       // 日本固定
        middleClassCode: state.conditions.area.middle, // 都道府県など
        smallClassCode: state.conditions.area.small,   // 市区町村など
        detailClassCode: state.conditions.area.detail, // 駅や詳細地域など
        formatVersion: '2',
        datumType: '1',                                // 世界測地系、単位は度
        hits: '10',                                    // 最大10件のレスポンスを得る(最大30件まで指定出来る)
        page: state.conditions.currentPage,            // どのページを起点に検索するかを指定する
        responseType: 'middle',                        // 中程度の情報量を返却することをリクエストする
        sort: 'standard'                               // レスポンスはキーワード適中率が高い順でソートする
      }
      const apiPath = '/Travel/SimpleHotelSearch/20170426'
      requestRakutenApi(apiPath, params)
        .then(data => {
          if (data !== null) {
            commit(types.UPDATE_RESULT_STATE, data)
          }
        })
    }
  }
}
export default HotelSearch
