'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  RAKUTEN_API_KEY: '"XXXXXXXXXXXXXXXXXXXX"' // 楽天ウェブサービスのAPIキーと置き換えて下さい
})
